const params = new URLSearchParams(location.search);
const boardID = params.get("id");
const key="6ec072eef050ec7be8981f0cc1fb128e";
const token="fae1ce16db4e108a3681db672b88daf1beade1d7b1bb7ee1e84e20b254de2843";
const listContainer=document.querySelector(".list-container")
//list
function fetchBoardName()
{

fetch(`https://api.trello.com/1/boards/${boardID}?key=${key}&token=${token}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    }
  })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.json();
    })
    .then(text =>{
        const boardName= document.querySelector(".board-name");
        boardName.innerText=text.name;
        
    }
        )
    .catch(err => console.error(err));

}
fetchBoardName()
function fetchList(){

    fetch(`https://api.trello.com/1/boards/${boardID}/lists?key=${key}&token=${token}`, {
        method: 'GET'
      })
        .then(response => {
          console.log(
            `Response: ${response.status} ${response.statusText}`
          );
          return response.json();
        })
        .then(listArray =>{
            addList(listArray);  
            
        } )
        .catch(err => console.error(err));
}

function addList(listArray)
{
    listArray.forEach(singleList=>{
        list(singleList);
        
        
    })
}


function createANewCard(list)
{
  const newCard=document.createElement("div")
  newCard.classList.add("new-card");
  newCard.innerText="+Add a card"
  const addCard=document.createElement("div")
  addCard.classList.add("createCard");
  const inputCardName=document.createElement("input");
  inputCardName.classList.add("inputCard");
  const addButton=document.createElement("button");
  addButton.classList.add("addButton")
  addButton.innerText="Add";

  addCard.appendChild(inputCardName);
  addCard.appendChild(addButton);
  list.appendChild(addCard)
  list.appendChild(newCard)
  newCard.addEventListener("click",(e)=> {
    if(e.target.classList.contains("new-card")){
      addCard.style.display="flex";
    } 

  })

  addCard.addEventListener("click", (e) => {
    if (e.target.classList.contains("addButton")){

      const cardName =  e.target.previousElementSibling;
      if(cardName.value) {
        createNewCard(cardName.value, e.target.parentNode.parentNode);
      }
      
      cardName.value = null;
      addCard.style.display="none";
    }
  })

}
function list(singleList){
    const list=document.createElement("div");
    list.classList.add("list");
    list.setAttribute("id", singleList.id);
    list.innerText=singleList.name;
    const delBtnList=document.createElement("button");
    delBtnList.classList.add("deleteList")
    delBtnList.innerText="X"
   fetchCards(singleList.id, list);
   list.append(delBtnList)
   createANewCard(list)
  delBtnList.addEventListener("click",()=>{
   
   delList(singleList.id)
  })
listContainer.appendChild(list)

}
fetchList()

function createList(listName)
{
    fetch(`https://api.trello.com/1/boards/${boardID}/lists?key=${key}&token=${token}&name=${listName}`, {
        method: 'POST'
      })
        .then(response => {
          console.log(
            `Response: ${response.status} ${response.statusText}`
          );
          return response.json();
        })
        .then(listNew => list(listNew))
        .catch(err => console.error(err));
}
//createList()

document.querySelector(".add-list").addEventListener("click", () => {
    document.querySelector(".popup-container").style.display = "flex";

  document.querySelector(".popup").addEventListener("click", (event) => {
    if(event.target.classList.contains("cancel")){
  
      document.querySelector(".popup-container").style.display = "none";
      const boardName = event.target.parentNode.previousElementSibling;
      boardName.value = null;
  
    } else if(event.target.classList.contains("create")){
      const boardName = event.target.parentNode.previousElementSibling;
      createList(boardName.value);
      boardName.value = null;
      document.querySelector(".popup-container").style.display = "none";
    }
  })
  })
  
  
  
  

  //cardsdocument.querySelector(".").addEventListener("click", () => {

  function fetchCards(listId, cardContainer) {
    
fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`, {
  method: 'GET'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(cardArray => {
    cardArray.forEach(card =>{ addfetchCards(card, cardContainer)
      checklistCreate(card)
     const fetchName=document.createElement("h4");
     fetchName.innerText=card.name;
     document.querySelector(".check-add").appendChild(fetchName);
    }
    );
  })
  .catch(err => console.error(err));
}

function addfetchCards(singleCard, cardContainer)
{
  

    const card=document.createElement("div");
    card.classList.add("cards");
    card.setAttribute("id",singleCard.id)
    const cardName=document.createElement("div");
    cardName.classList.add("cardName");
    cardName.innerText=singleCard.name;
    const delCard=document.createElement("button");
    delCard.classList.add("delCard");
    delCard.innerText="X"
    card.append(cardName);
    card.append(delCard);
    cardContainer.appendChild(card);
   

}
function checklistCreate(singleCard)
{
   //adding checklistchecklistCreate(card)

   document.querySelector(".checklist").addEventListener("click",(e)=>{
    const name = e.target.previousElementSibling;
    // console.log(name);
    addChecklist(singleCard.id, name.value)
    name.value = null;
    
  })
 

document.getElementById(`${singleCard.id}`).addEventListener("click",(e)=>{
  if(e.target.classList.contains("delCard")){
    deleteCard(singleCard.id)
    
  }
  else{
document.querySelector(".container").style.display = "flex";
fetchChecklist(singleCard.id);

document.querySelector(".close").addEventListener("click",()=>{
  document.querySelector(".container").style.display = "none";
})
  }
})

}



function createNewCard(cardName, list) {
  fetch(
    `https://api.trello.com/1/cards?key=${key}&token=${token}&idList=${list.id}&name=${cardName}`,
    { method: "POST" }
  )
    .then((response) => {
      // console.log(response);
      return response.json();
    })
    .then((cardInfo) => {
      addfetchCards(cardInfo, list);
      location.reload();
    })
    .catch((err) => {
      console.log(err);
    });
}

// checklist
function fetchChecklist(cardId)
{
   
fetch(`https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`, {
  method: 'GET'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text =>{
    text.forEach(checklist=>{
      createChecklist(checklist)
      fetchCheckitem(checklist.id)

document.querySelector(".addItem").addEventListener("click",(e)=>{
  const checkitemName=e.target.previousElementSibling.value
  addCheckItem(checklist.id,checkitemName)
  e.target.previousElementSibling.value=null
})
        
      document.getElementById(`${checklist.id}`).addEventListener("click",()=>{
        delChecklist(checklist.id)
        
       
      })
    })
  } )
  .catch(err => console.error(err));
}

function createChecklist(singlecheckList)
{
  const check=document.querySelector(".check-add")
      const singleChecklist=document.createElement("div")
      singleChecklist.classList.add("singleChecklist")
      
      const checkCon=document.createElement("div")
      checkCon.classList.add("checkCon")
      const singleCheck=document.createElement("div")
      singleCheck.classList.add("checkName");
      const delCheck=document.createElement("button")
      delCheck.classList.add("delChecklist");
      delCheck.setAttribute("id",singlecheckList.id)
      delCheck.innerText="X";
      const checkItem=document.createElement("div")
      checkItem.classList.add("checkItem")
      const itemInput=document.createElement("input")
      itemInput.classList.add("inputItem");
      const addItem=document.createElement("button")
      addItem.classList.add("addItem");
      addItem.innerText="Add Item"
      
checkItem.appendChild(itemInput)
checkItem.appendChild(addItem)
      singleCheck.innerText=singlecheckList.name
      
      checkCon.appendChild(singleCheck);
      checkCon.appendChild(delCheck);
      singleChecklist.appendChild(checkCon)
      singleChecklist.appendChild(checkItem)
      check.appendChild(singleChecklist)
    
}


function addChecklist(cardId,checkName)
{
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}&name=${checkName}`,
    { method: "POST" }
  )
    .then((response) => {
      return response.json();
    })
    .then((checklist) => {  
      createChecklist(checklist)
      
    });
}



//delete a list
function delList(listId)
{

  // console.log(listId);

fetch(`https://api.trello.com/1/lists/${listId}/closed?key=${key}&token=${token}&value=true`, {
  method: 'PUT'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => document.getElementById(`${listId}`).remove())
  .catch(err => console.error(err));
}

//delete card
function deleteCard(cardId)
{

fetch(`https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`, {
  method: 'DELETE'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => document.getElementById(`${cardId}`).remove())
  .catch(err => console.error(err));
}

//delete checklists
function delChecklist(checkId){
  fetch(
    `https://api.trello.com/1/checklists/${checkId}?key=${key}&token=${token}`,
    { method: "DELETE" }
  ).then((response) => {
    return response.json();
  })
  .then(text=>{
    if(document.getElementById(`${checkId}`).parentNode.nextSibling.nextSibling)
    document.getElementById(`${checkId}`).parentNode.nextSibling.nextSibling.remove();
    document.getElementById(`${checkId}`).parentNode.nextSibling.remove();
    document.getElementById(`${checkId}`).parentNode.remove();
  
  })
  .catch(err => console.error(err));
}

// checklist item

function fetchCheckitem(checklistId){
  console.log(checklistId);

fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${key}&token=${token}`, {
  method: 'GET'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => 
    text.forEach(singleCheckItem=>{
      console.log(singleCheckItem)
      createCheckItem(singleCheckItem)
    })
  )
  .catch(err => console.error(err));
}

function addCheckItem(checkListId,name){

fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${key}&token=${token}&name=${name}`, {
  method: 'POST'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(singleCheckItem =>createCheckItem(singleCheckItem) )
  .catch(err => console.error(err));
}

function createCheckItem(singleCheckItem)
{
   const checkitemContainer=document.querySelector(".singleChecklist")
   const checkbox=document.createElement("input")
   const checkListItem=document.createElement("div")
   checkListItem.classList.add("checkitemcontainer");
   checkbox.setAttribute("type","checkbox")
   const checkItem=document.createElement("div")
   checkItem.classList.add("checkitem");
   checkItem.innerText=singleCheckItem.name;
   checkItem.setAttribute("id",singleCheckItem.id);
   checkitemContainer.appendChild(checkListItem)
   checkListItem.appendChild(checkbox)
   checkListItem.appendChild(checkItem)
}
